#!/bin/bash
# Simple setup.sh for configuring Ubuntu 12.04 LTS at Webly

#normal webly convention


cd $HOME
mkdir projects
cd projects
mkdir www
cd www
touch info.php
echo "<?php phpinfo(); ?>" >> info.php
mkdir webly
cd $HOME




# install open dns change your ip to 192.168.2.<your_number>


#install git curl
sudo apt-get install -y git
sudo apt-get install -y curl

#Install node
sudo apt-get install -y python-software-properties python g++ make
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get -qq update
sudo apt-get install -y nodejs
# Install jshint to allow checking of JS code within emacs
# http://jshint.com/
npm install -g jshint

# Install rlwrap to provide libreadline features with node
# See: http://nodejs.org/api/repl.html#repl_repl
sudo apt-get install -y rlwrap

# Install emacs24
# https://launchpad.net/~cassou/+archive/emacs
sudo add-apt-repository -y ppa:cassou/emacs
sudo apt-get -qq update
sudo apt-get install -y emacs24-nox emacs24-el emacs24-common-non-dfsg

# Install sublime text 2
sudo add-apt-repository -y ppa:webupd8team/sublime-text-2
sudo apt-get -qq update
sudo apt-get install -y sublime-text

# Install openssh client and server

sudo apt-get install -y openssh-server

# Install ruby 
sudo apt-get install -y ruby
# Install ruby gems
sudo apt-get install -y rubygems


# Install sass 
gem install sass


# Install less
npm install -g less

# Install php5 appache2 etc from http://www.howtoforge.com/using-php5-fpm-with-apache2-on-ubuntu-12.04-lts

# mysql-5
sudo apt-get install -y mysql-server mysql-client

#appache-2
sudo apt-get install -y apache2-mpm-worker

#php5
sudo apt-get install -y libapache2-mod-fastcgi php5-fpm php5
sudo a2enmod actions fastcgi alias
#sudo /etc/init.d/apache2 restart

echo "<?php phpinfo(); ?>" >> /var/www/info.php

sudo apt-cache search php5

sudo apt-get install -y php5-mysql php5-curl php5-gd php5-intl php-pear php5-imagick php5-imap php5-mcrypt php5-memcache php5-ming php5-ps php5-pspell php5-recode php5-snmp php5-sqlite php5-tidy php5-xmlrpc php5-xsl


sudo apt-get install -y phpmyadmin
#########################################################################################################################





# # git pull and install dotfiles as well
# cd $HOME
# if [ -d ./weblydotfiles/ ]; then
#     mv weblydotfiles weblydotfiles.old
# fi
# if [ -d .emacs.d/ ]; then
#     mv .emacs.d .emacs.d~
# fi
# git clone https://hulkwebly@bitbucket.org/hulkwebly/weblydotfiles.git
# ln -sf weblydotfiles/.emacs.d .


